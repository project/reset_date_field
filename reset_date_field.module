<?php

/**
 * @file
 * Contains reset_date_field.module.
 */

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\reset_date_field\Ajax\ResetDateCommand;

/**
 * Implements hook_help().
 */
function reset_date_field_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the reset_date_field module.
    case 'help.page.reset_date_field':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('&#039;&#039;Provide widget &#039;&#039;&#039;&#039;Date Time Reset&#039;&#039;&#039;&#039; to reset datetime field values.&#039;&#039;') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_form_alter().
 */
function reset_date_field_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  $form_object = $form_state->getFormObject();
  if ($form_object instanceof ContentEntityForm) {
    if ($form_object->getEntity()->getEntityTypeId() == 'node') {
      $entity = $form_object->getEntity();
      $bundle = $entity->bundle();
      $entity_type = $entity->getEntityTypeId();
      if (!empty($entity_type) && !empty($bundle)) {
        $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type, $bundle);
        foreach ($field_definitions as $fieldDefinition) {
          if (($fieldDefinition->getType() == 'datetime') || ($fieldDefinition->getType() == 'timestamp')) {
            $form['#attached']['library'][] = 'reset_date_field/reset_date_field-library';
            break;
          }
        }
      }
    }
  }
}

/**
 * Ajax submit ajax handler form.
 *
 * @param array $form
 *   From array Object.
 * @param Drupal\Core\Form\FormStateInterface $form_state
 *   Form State object.
 *
 * @return Drupal\Core\Ajax\AjaxResponse
 *   Return ajax response.
 */
function _reset_date_time_form_submit(array $form, FormStateInterface $form_state) {
  $response = new AjaxResponse();
  // Getting the argument from field.
  $element = $form_state->getTriggeringElement()['#name'];
  $string = str_replace('_', '-', str_replace('reset_date_time_', '', $element));
  // Creating field Ids.
  $date = '#edit-' . $string . '-0-value-date';
  $time = '#edit-' . $string . '-0-value-time';

  // Custom ajax command to reset field.
  $response->addCommand(new ResetDateCommand($element, NULL, [
    'date' => $date,
    'time' => $time,
  ]));

  return $response;
}
