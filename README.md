# Reset Date Field

Reset Date Field module provides the "Reset" button just next/below Date Field.
On click on reset button date, date time, created date get rest with default
empty.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/reset_date_field).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/reset_date_field).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable this module like any other module in drupal.
   `Drush en reset_date_field`
2. Once the module is enabled, go to content type manage form display setting.
3. Select the widget type "Reset Date Field Widget" type.


## Usage:

This module provides the "Reset" button next to the date field, On click on
reset button date field get reset and default empty.


## Maintainers

- Chandravilas Kute - [chandravilas](https://www.drupal.org/u/chandravilas)
