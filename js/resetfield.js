/**
* @file
*/

(function ($, Drupal) {
Drupal.AjaxCommands.prototype.reset = function (ajax, response, status) {
  // Empty the date field.
  if (response.args && response.args.date) {
    $(response.args.date).val('');
  }
  // Empty the time field.
  if (response.args && response.args.time) {
    $(response.args.time).val('');
  }
}

})(jQuery, Drupal);
